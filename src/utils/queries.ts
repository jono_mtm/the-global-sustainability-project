import { useStaticQuery, graphql } from "gatsby";
import { Business, Category, Post } from "./types";

const useData = () => {
  const data = useStaticQuery(graphql`
    {
      allGraphCmsCategory {
        edges {
          node {
            id
            title
            posts {
              id
              createdAt
              title
              featuredImage {
                url
              }
              content {
                markdown
              }
              category {
                id
                title
              }
              tags
            }
          }
        }
      }
      allGraphCmsPost {
        edges {
          node {
            id
            createdAt
            title
            featuredImage {
              url
            }
            content {
              markdown
            }
            category {
              id
              title
            }
            tags
          }
        }
      }
      allGraphCmsSustainableBusiness {
        edges {
          node {
            id
            link
            name
            logo {
              url
            }
            tags
            description
          }
        }
      }
    }
  `);
  const categories: Category[] = data.allGraphCmsCategory.edges.map(
    (category) => category.node
  );
  const businesses: Business[] = data.allGraphCmsSustainableBusiness.edges.map(
    (business) => business.node
  );
  const posts: Post[] = data.allGraphCmsPost.edges.map((post) => post.node);
  return { categories, businesses, posts };
};
export const useCategories: () => Category[] = () => {
  const { categories } = useData();
  return categories;
};

export const usePosts: () => Post[] = () => {
  const { posts } = useData();
  return posts;
};

export const useBusinesses: () => Business[] = () => {
  const { businesses } = useData();
  return businesses;
};
