import { Slider } from "antd";
import { graphql, useStaticQuery } from "gatsby";
import Markdown from "markdown-to-jsx";
import React, { useEffect, useState } from "react";

import HighImg from "../../images/intro/high.svg";
import LowImg from "../../images/intro/low.svg";
import MedImg from "../../images/intro/med.svg";
import styles from "./css/quiz.module.scss";

const Quiz: React.FC = (props: any) => {
  const data = useStaticQuery(graphql`
    query {
      allGraphCmsPage(
        filter: { parentPage: { title: { eq: "How Green are you?" } } }
      ) {
        edges {
          node {
            order
            id
            title
            description {
              markdown
            }
          }
        }
      }
    }
  `);
  const [titles] = useState([""]);
  const [Idx, setIdx] = useState(0);
  const [descriptions] = useState(["Drag the Slider to position yourself"]);
  const images = ["", LowImg, MedImg, HighImg];

  useEffect(() => {
    data.allGraphCmsPage.edges.forEach((page) => {
      // Find out page order by Index, and push in order.
      for (let i = 0; i < data.allGraphCmsPage.edges.length; i++) {
        if (i === page.node.order) {
          titles.push(data.allGraphCmsPage.edges[i].node.title);
          descriptions.push(
            data.allGraphCmsPage.edges[i].node.description.markdown
          );
        }
      }
    });
  }, [data, descriptions, titles]);

  const handleChange = (value) => {
    setIdx(value);
    if (value == 0) {
      props.setButton3Enabled();
    } else {
      props.setButton3Disabled();
    }
  };

  return (
    <section className={styles.container}>
      <div className={styles.imgWrapper}>
        <img className={styles.img} src={images[Idx]} alt={titles[Idx]} />
      </div>
      <Slider
        // className={styles.slider}
        min={0}
        max={3}
        value={Idx}
        onChange={handleChange}
        style={{ pointerEvents: "auto" }}
      />
      <h2 className={styles.title}>{titles[Idx]}</h2>
      <Markdown className={styles.content}>{descriptions[Idx]}</Markdown>
    </section>
  );
};

export default Quiz;
