import React from "react";
import Markdown from "markdown-to-jsx";

import styles from "./css/introlayout.module.scss";
import Footer from "./Footer";
import Header from "./Header";

const IntroLayout: React.FC = (props: any) => (
  <main className={styles.main}>
    <Header />
    <main
      className={styles.container}
      style={{ marginLeft: props.welcomePage && "80px" }}
    >
      <Markdown
        className={styles.title}
        style={{ textAlign: props.welcomePage && "left" }}
      >
        {props.title || ""}
      </Markdown>
      <Markdown
        className={styles.content}
        style={{
          textAlign: props.welcomePage && "left",
          marginLeft: props.welcomePage && "0",
        }}
      >
        {props.content || ""}
      </Markdown>
      {props.children}
    </main>
    <div
      className={styles.bg}
      style={{ backgroundImage: `url("${props.bg}")` }}
    />
    <Footer
      buttonText1={props.buttonText1}
      buttonLink1={props.buttonLink1}
      buttonText2={props.buttonText2}
      buttonLink2={props.buttonLink2}
      button2Disabled={props.button2Disabled}
      currentStep={props.currentStep}
      buttonText3={props.buttonText3}
      buttonLink3={props.buttonLink3}
      button3Disabled={props.button3Disabled}
      welcomePage={props.welcomePage}
    />
  </main>
);

export default IntroLayout;
