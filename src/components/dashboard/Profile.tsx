import React from "react";

import CompletedTasks from "../ui/CompletedTasks";
import UserModule from "../ui/UserModule";
import styles from "./css/profile.module.scss";

const Profile: React.FC = (props: any) => (
  <section className={styles.section}>
    <UserModule />
    <CompletedTasks setBadge={props.setBadge} />
  </section>
);

export default Profile;
