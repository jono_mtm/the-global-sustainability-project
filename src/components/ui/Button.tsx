import { Link } from "gatsby";
import React from "react";

import styles from "./css/button.module.scss";

interface Props {
  green?: boolean;
  disabled?: boolean;
  onClick?: () => void;
  type?: "button" | "submit" | "reset" | undefined;
  text: string;
  to?: string;
}

const Button: React.FC<Props> = ({
  green,
  disabled,
  onClick,
  type,
  to,
  text,
}) => (
  <div className={styles.container}>
    <div className={green ? styles.green : ""}>
      <div className={disabled ? styles.disabled : ""}>
        {onClick || type ? (
          <button onClick={onClick} type={type} className={styles.button}>
            {text}
          </button>
        ) : (
          <Link to={to || ""} className={styles.button}>
            {text}
          </Link>
        )}
      </div>
    </div>
  </div>
);

export default Button;
