import { Link } from "gatsby";
import React from "react";

import styles from "./css/navlink.module.scss";
import Dropdown from "./Dropdown";

interface Props {
  to: string;
  state?: any;
  active?: boolean;
  text?: string;
  learnData?: any;
}

const NavLink: React.FC<Props> = ({ to, state, active, text, learnData }) => (
  <Link className={styles.navLink} to={to} state={state}>
    <div className={active ? styles.active : undefined}>
      <span className={styles.text}>{text}</span>
      {learnData && <Dropdown />}
    </div>
  </Link>
);

export default NavLink;
