import axios from "axios";
import { Field, Form, Formik } from "formik";
import { Link } from "gatsby";
import React, { useState } from "react";

import Button from "./Button";
import styles from "./css/signupform.module.scss";
import Input from "./Input";

const SignUpForm = () => {
  const [error, setError] = useState(null);
  return (
    <Formik
      initialValues={{ username: "", email: "", password: "" }}
      // onSubmit={async values => {
      //   // Signup + Create User
      //   try {
      //     setError(null)
      //     const body = values
      //     console.log(values)
      //     var {
      //       data: { token },
      //     } = await axios.post(`/jwt-auth/v1/token`, {
      //       username: process.env.GATSBY_WP_ADMIN_USERNAME,
      //       password: process.env.GATSBY_WP_ADMIN_PASS,
      //     })
      //     console.log(token)
      //     // FIXME?
      //     await axios.post(`/wp/v2/users`, body, {
      //       headers: {
      //         authorization: `Bearer ${token}`,
      //       },
      //     })
      //     console.log("posted")
      //     typeof window !== `undefined` &&
      //       window.sessionStorage.setItem("jwt", token)
      //     console.log("jwt set")

      //     // INSERT LOGIN HERE
      //     try {
      //       // LOGIN
      //       var {
      //         data: { token },
      //       } = await axios.post(`/jwt-auth/v1/token`, {
      //         username: values.username,
      //         password: values.password,
      //       })
      //       console.log(token)
      //       typeof window !== `undefined` &&
      //         window.sessionStorage.setItem("jwt", token)
      //       console.log("jwt set")
      //       navigate("/")
      //     } catch (err) {
      //       console.log(err.response)
      //       setError(err.response.data.message)
      //     }
      //   } catch (err) {
      //     console.log(err)
      //     setError(err.response.data.message)
      //     // IF EMAIL IS MISSING, TRY LOGIN
      //     if (err.response.data.code === "rest_invalid_param") {
      //       try {
      //         setError(null)
      //         console.log("trying login")
      //         // LOGIN
      //         const {
      //           data: { token },
      //         } = await axios.post(`/jwt-auth/v1/token`, {
      //           username: values.username,
      //           password: values.password,
      //         })
      //         // could try pass in "="
      //         console.log(values)
      //         typeof window !== `undefined` &&
      //           window.sessionStorage.setItem("jwt", token)
      //         console.log("jwt set")
      //         navigate("/")
      //       } catch (err) {
      //         console.log(err.response)
      //         setError(err.response.data.message)
      //       }
      //     }
      //   }
      // }}
    >
      <Form>
        <div className={styles.form}>
          <Field
            as={Input}
            tabIndex={0}
            text="Username"
            name="username"
            placeholder="JohnDoe1"
          />
          <Field
            as={Input}
            tabIndex={0}
            text="Email"
            name="email"
            placeholder="john.doe@gmail.com"
          />
          <Field
            as={Input}
            tabIndex={1}
            text="Password"
            name="password"
            placeholder="Type your Password"
          />
          <div
            dangerouslySetInnerHTML={{ __html: error }}
            style={{ width: "250px", textAlign: "center" }}
          />
          <Button type="submit" text="Sign Up/Log In" green />
        </div>
      </Form>
    </Formik>
  );
};

export default SignUpForm;
