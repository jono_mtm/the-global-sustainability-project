import React from "react";

import TierBadge from "../../images/profile/tier.svg";
import { useUser } from "../../utils/useUser";
import Button from "./Button";
import styles from "./css/usermodule.module.scss";

const UserModule: React.FC = () => {
  const user = useUser();
  return (
    <div className={styles.module}>
      {user ? (
        <>
          <div
            className={styles.avatar}
            style={{ backgroundImage: `url("${user.avatar_urls[96]}")` }}
          >
            <div className={styles.tierBadge}>
              <p>{user.acf.tier || "No Tier!"}</p>
              <img src={TierBadge} alt="Tier Badge" />
            </div>
          </div>

          <h2 className={styles.name}>{user.name}</h2>
          <h4 className={styles.tier}>{user.acf.tier}</h4>
          {/* <p>Edit Profile</p> */}
          <Button
            onClick={() => {
              typeof window !== "undefined" &&
                window.sessionStorage.removeItem("jwt");
              typeof window !== "undefined" && window.location.reload();
            }}
            text="Log Out"
          />
        </>
      ) : (
        <>You must sign in to view your Profile</>
      )}
    </div>
  );
};

export default UserModule;
