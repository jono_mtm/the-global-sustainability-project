import styled from "@emotion/styled";
import { Avatar, Card, Col, Row } from "antd";
import Paragraph from "antd/lib/typography/Paragraph";
import Text from "antd/lib/typography/Text";
import Title from "antd/lib/typography/Title";
import { Link, navigate } from "gatsby";
import { truncate } from "lodash";
import Markdown from "markdown-to-jsx";
import React from "react";
import { slugify } from "../../utils/helpers";
import { Business, Category, Post } from "../../utils/types";
import Tag from "./Tag";
import moment from "moment";
import {
  AppstoreOutlined,
  FileTextOutlined,
  ShopOutlined,
} from "@ant-design/icons";

const StyledCard = styled.div<{ background?: string }>`
  display: flex;
  background: ${(props) => props.background || "#fefefe"};
  align-items: center;
  border: ${(props) => props.background || "1px solid #eee"};
  border-radius: 4px;
  margin: 10px 0;
  padding: 10px;
  width: 100%;
  cursor: pointer;
  &:hover {
    border-color: #27ae60;
  }
  & > * {
    margin: 10px;
  }
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  & > * {
    margin: 5px;
  }
`;

const Banner = styled.div`
  width: 300px;
  height: 200px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

export const BusinessCard: React.FC<{ business: Business }> = ({
  business,
}) => {
  if (!business) return null;
  const { name, link, description, tags, logo } = business;
  return (
    <a href={link} target="_blank" rel="noopener noreferrer">
      <StyledCard>
        {logo && (
          <Avatar
            size={100}
            style={{ display: "table", margin: "0 20px" }}
            src={logo.url}
          />
        )}
        <Content>
          <Text type="secondary">
            <ShopOutlined /> Sustainable Business
          </Text>
          <Title level={3}>{name}</Title>
          <Paragraph>{truncate(description, { length: 200 })}</Paragraph>
          <Row>
            {tags
              ?.filter((tag, i) => i < 5)
              .map((tag) => (
                <Tag key={tag} tag={tag} />
              ))}
          </Row>
        </Content>
      </StyledCard>
    </a>
  );
};

export const CategoryCard: React.FC<{
  category: Category;
  condensed?: boolean;
}> = ({ category, condensed }) => {
  if (!category) return null;

  const { title, posts } = category;

  if (condensed)
    return (
      <Link to="/learn" state={{ category: title }}>
        {title}
      </Link>
    );

  return (
    <Link to="/learn" state={{ category: title }}>
      <StyledCard>
        <Content>
          <Text type="secondary" style={{ fontSize: 12 }}>
            <AppstoreOutlined /> Category
          </Text>
          <Title level={3}>{title}</Title>
          {posts?.map((post) => (
            <StyledCard
              background="#eee"
              key={post.id}
              // onClick={() =>
              //   navigate(
              //     `/${slugify(
              //       post.category ? post.category[0].title : ""
              //     )}/${slugify(post.title)}`
              //   )
              // }
            >
              <Banner
                style={{
                  backgroundImage: `url("${post.featuredImage?.url}`,
                  display: "table", // keeps tags display consistent
                  height: 80,
                  width: 100,
                }}
              />
              <Content>
                <Title level={3}>{post.title}</Title>
                <span>
                  <Text type="secondary" style={{ fontSize: 12 }}>
                    {moment(post.createdAt).format("ll")}{" "}
                  </Text>
                  <Markdown>
                    {truncate(post.content?.markdown, { length: 100 })}
                  </Markdown>
                </span>
              </Content>
              <Row style={{ width: "max-content", display: "contents" }}>
                {post.tags
                  ?.filter((tag, i) => i < 2 /*Display first 3 tags*/)
                  .map((tag) => (
                    <Tag key={tag} tag={tag} />
                  ))}
              </Row>
            </StyledCard>
          ))}
        </Content>
      </StyledCard>
    </Link>
  );
};

export const PostCard: React.FC<{ condensed?: boolean; post: Post }> = ({
  condensed,
  post,
}) => {
  if (!post) return null;

  const { title, tags, featuredImage, createdAt } = post;

  const category = post.category && post.category[0];
  const content = post.content?.markdown;

  const slug = `/${slugify(category?.title || "")}/${slugify(title)}`;

  if (condensed)
    return (
      <StyledCard onClick={() => navigate(slug)}>
        <div>
          <Title level={3}>
            <FileTextOutlined /> {title}
          </Title>
          <Text>
            {moment(createdAt).format("ll")} in{" "}
            {category && <CategoryCard condensed category={category} />}
          </Text>
          <Markdown>{truncate(content, { length: 50 })}</Markdown>
        </div>
      </StyledCard>
    );

  return (
    <StyledCard onClick={() => navigate(slug)}>
      <Content>
        <Text type="secondary">
          <FileTextOutlined /> Article
        </Text>
        <Title level={3}>{title}</Title>
        <Text>
          {moment(createdAt).format("ll")} in{" "}
          {category && <CategoryCard condensed category={category} />}
        </Text>
        <Markdown>{truncate(content, { length: 150 })}</Markdown>
        <Row>
          {tags?.map((tag) => (
            <Tag key={tag} tag={tag} />
          ))}
        </Row>
      </Content>
      <Banner style={{ backgroundImage: `url("${featuredImage?.url}` }} />
    </StyledCard>
  );
};
