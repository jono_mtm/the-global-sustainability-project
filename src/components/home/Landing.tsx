import React from "react";

import { useUser } from "../../utils/useUser";
import CondensedDashboard from "../ui/CondensedDashboard";
import Fact from "./Fact";

const Landing: React.FC = () => {
  const user = useUser();
  return (
    <>
      <Fact />
      {user && <CondensedDashboard user={user} />}
    </>
  );
};

export default Landing;
