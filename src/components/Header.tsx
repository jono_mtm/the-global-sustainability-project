import React from "react";

import styles from "./css/header.module.scss";
import Nav from "./Nav";
import Logo from "./ui/Logo";
import ProfileButton from "./ui/ProfileButton";
import Search from "./ui/Search";

interface Props {
  active?: string;
}

const Header: React.FC<Props> = ({ active }) => (
  <header className={styles.header}>
    <Logo />
    <Nav active={active} />
    <div className={styles.rightAligned}>
      <Search />
      {/* <ProfileButton /> */}
    </div>
  </header>
);

export default Header;
