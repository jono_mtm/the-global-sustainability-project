import React from "react";

import Landing from "../components/home/Landing";
import Layout from "../components/Layout";

const Home = () => (
  <Layout active="home">
    <Landing />
  </Layout>
);

export default Home;
