import React from "react";

import Layout from "../components/Layout";
import Search from "../components/ui/Search";

const All = () => (
  <Layout>
    <h1>Search</h1>
    <Search />
  </Layout>
);
export default All;
