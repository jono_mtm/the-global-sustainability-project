import { graphql } from "gatsby";
import React, { useState } from "react";

import IntroLayout from "../components/intro/IntroLayout";
import Quiz from "../components/intro/Quiz";
import bg from "../images/intro/bg-intro1.svg";

const Intro1 = ({ data }) => {
  const [button3Disabled, setButton3Disabled] = useState(true);
  const page = data?.graphCmsPage;
  if (!page) return null;
  console.log(page);
  return (
    <IntroLayout
      buttonText1="Skip"
      buttonLink1="/signup/"
      buttonText2="Back"
      buttonLink2="/intro/"
      button2Disabled
      currentStep="1"
      buttonText3="Next"
      buttonLink3="/intro2/"
      button3Disabled={button3Disabled}
      title={page?.title}
      content={page?.description?.markdown}
      bg={bg}
      children={
        <main>
          <Quiz
            setButton3Disabled={() => setButton3Disabled(false)}
            setButton3Enabled={() => setButton3Disabled(true)}
          />
        </main>
      }
    />
  );
};

export default Intro1;

export const query = graphql`
  query {
    graphCmsPage(
      parentPage: { title: { eq: "Introduction" } }
      order: { eq: 1 }
    ) {
      title
      description {
        markdown
      }
    }
  }
`;
