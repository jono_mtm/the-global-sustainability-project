import { graphql } from "gatsby";
import React from "react";

import IntroLayout from "../components/intro/IntroLayout";
import bg from "../images/intro/bg-intro.svg";

const Intro = ({ data }) => {
  const page = data?.graphCmsPage;
  if (!page) return null;
  return (
    <IntroLayout
      welcomePage
      buttonText1="I'm Ready"
      buttonLink1="/"
      buttonText2="Teach Me"
      buttonLink2="/intro1/"
      title={page?.title}
      content={page?.description.markdown}
      bg={bg}
    />
  );
};

export default Intro;

export const query = graphql`
  query {
    graphCmsPage(
      parentPage: { title: { eq: "Introduction" } }
      order: { eq: 0 }
    ) {
      title
      description {
        markdown
      }
    }
  }
`;
