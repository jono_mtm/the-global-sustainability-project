import { Link, graphql } from "gatsby";
import React from "react";

import IntroLayout from "../components/intro/IntroLayout";
import SignUpForm from "../components/ui/SignUpForm";
import styles from "../css/signup.module.scss";
import bg from "../images/intro/bg-signup.svg";
import Img from "../images/intro/signup.svg";

const SignUp = ({ data }) => {
  const page = data?.graphCmsPage;
  return (
    <IntroLayout
      buttonText1="Back"
      buttonLink1="intro2"
      buttonText3="Join Later"
      buttonLink3=""
      currentStep="3"
      title={page?.title}
      content={page?.description.markdown}
      bg={bg}
      children={
        <main className={styles.main}>
          <img className={styles.img} src={Img} alt="Sign Up" />
          <Link to="/">Join Later</Link>
          <SignUpForm />
        </main>
      }
    />
  );
};

export default SignUp;

export const query = graphql`
  query {
    graphCmsPage(
      parentPage: { title: { eq: "Introduction" } }
      order: { eq: 3 }
    ) {
      title
      description {
        markdown
      }
    }
  }
`;
