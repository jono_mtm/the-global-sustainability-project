import React from "react";

import Layout from "../components/Layout";

export default () => (
  <Layout>
    <div>Not Found</div>
  </Layout>
);
