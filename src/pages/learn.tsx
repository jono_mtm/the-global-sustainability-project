import React, { useEffect, useState } from "react";

import Layout from "../components/Layout";
import Feed from "../components/learn/Feed";
import Tabs from "../components/learn/Tabs";
import styles from "../css/learn.module.scss";
import { useCategories } from "../utils/queries";

const Learn = ({ location }) => {
  const categories = useCategories();

  const [currentCategory, setCurrentCategory] = useState<string>(
    categories[0]?.title
  );

  console.log(location.state, currentCategory);

  useEffect(() => {
    if (location?.state?.category) setCurrentCategory(location.state.category);
  }, [location]);

  return (
    <Layout active="learn">
      <Tabs
        currentCategory={currentCategory}
        onClick={(category) => setCurrentCategory(category)}
      />
      <section className={styles.section}>
        <Feed currentCategory={currentCategory} />
      </section>
    </Layout>
  );
};

export default Learn;
