import { Col, Row } from "antd";
import React, { useEffect, useState } from "react";
import Layout from "../components/Layout";
import { BusinessCard, CategoryCard, PostCard } from "../components/ui/Card";
import Input from "../components/ui/Input";
import Tag from "../components/ui/Tag";
import { useBusinesses, useCategories, usePosts } from "../utils/queries";

const Search: React.FC<{ location: any }> = ({ location }) => {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [tag, setTag] = useState<string | undefined>(location.state?.tag);

  const categories = useCategories();
  const posts = usePosts();
  const businesses = useBusinesses();

  const categoryHits = categories.filter((category) => {
    if (category.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)
      return true;
  });
  const postHits = posts.filter((post) => {
    if (post.title.toLowerCase().indexOf(searchTerm) > -1) return true;
    if (post.tags?.some((tag) => tag.indexOf(searchTerm.toLowerCase()) > -1))
      return true;
    if (tag && post.tags?.includes(tag)) return true;
  });
  const businessHits = businesses.filter((business) => {
    if (business.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1)
      return true;
    if (
      business.tags?.some((tag) => tag.indexOf(searchTerm.toLowerCase()) > -1)
    )
      return true;
    if (tag && business.tags?.includes(tag)) return true;
  });

  return (
    <Layout active="search">
      <Row justify="center">
        <Input
          placeholder="Search..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
      </Row>

      {tag && (
        <Row justify="center">
          <Tag onClick={() => setTag(undefined)} tag={tag} />
        </Row>
      )}

      <Row>
        {postHits.length === 0 &&
          categoryHits.length === 0 &&
          businessHits.length === 0 && (
            <div style={{ marginTop: 20 }}>
              <h3>No results! Try a different term.</h3>
            </div>
          )}
      </Row>

      {postHits?.map((post) => (
        <PostCard key={post.id} post={post} />
      ))}

      {categoryHits?.map((category) => (
        <CategoryCard key={category.id} category={category} />
      ))}

      {businessHits?.map((business) => (
        <BusinessCard key={business.id} business={business} />
      ))}
    </Layout>
  );
};

export default Search;
