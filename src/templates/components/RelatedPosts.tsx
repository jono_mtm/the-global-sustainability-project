import { intersection } from "lodash";
import React from "react";
import { PostCard } from "../../components/ui/Card";
import { usePosts } from "../../utils/queries";
import styles from "./css/relatedposts.module.scss";

const RelatedPosts: React.FC<{ tags: string[] }> = ({ tags }) => {
  const posts = usePosts();

  const relatedPosts = posts.filter(
    (post) => intersection(post.tags, tags).length > 0
  );

  if (relatedPosts.length < 1) return null;

  return (
    <div className={styles.relatedPosts}>
      <h3 className={styles.title}>Related Posts</h3>
      {relatedPosts
        .filter((post, i) => i < 3)
        .map((post) => (
          <PostCard key={post.id} post={post} />
        ))}
    </div>
  );
};

export default RelatedPosts;
