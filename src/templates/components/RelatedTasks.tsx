import React from "react";

import Badge from "../../components/ui/Badge";
import { useAllTasks } from "../../utils/useAllTasks";
import styles from "./css/relatedtasks.module.scss";

const RelatedTasks: React.FC = (props: any) => {
  const { post } = props;
  const tasks = useAllTasks();

  if (!tasks) return null;

  const relatedTasks = tasks.filter((task) =>
    post.relatedtasks?.some((relatedTask) => relatedTask?.id === task.id)
  );

  if (!relatedTasks) return null;

  return (
    <>
      <h3 className={styles.title}>Related Tasks</h3>
      <div className={styles.relatedTasks}>
        {relatedTasks.map((task) => (
          <Badge key={task} badge={task} complete compact />
        ))}
      </div>
    </>
  );
};

export default RelatedTasks;
