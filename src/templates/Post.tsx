import styled from "@emotion/styled";
import { Divider, Row } from "antd";
import { Link, graphql } from "gatsby";
import Markdown from "markdown-to-jsx";
import moment from "moment";
import React from "react";

import Layout from "../components/Layout";
import Search from "../components/ui/Search";
import Tag from "../components/ui/Tag";
import RelatedPosts from "./components/RelatedPosts";
import styles from "./css/post.module.scss";
import Typography from "antd/lib/typography";

const { Text } = Typography;

const Banner = styled.div`
  position: absolute;
  width: 100vw;
  top: 100px; // height of header
  left: 0;
  height: 400px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`;

const Header = ({ post }) => (
  <header>
    <h1 className={styles.title}>{post.title}</h1>
    <Text type="secondary">{moment(post.createdAt).format("ll")}</Text> in{" "}
    <Link to="/learn">
      <a type="success" className={styles.categories}>
        {post.category[0]?.title}
      </a>
    </Link>
    {post.tags.length > 0 && (
      <Row align="middle">
        Tags:{" "}
        {post.tags.map((tag) => (
          <Tag key={tag} tag={tag} />
        ))}
      </Row>
    )}
    <Divider />
  </header>
);

const Footer = ({ post }) => (
  <footer className={styles.footer}>
    <div className={styles.learn}>
      <h3>Learn something new today.</h3>
    </div>
    <div>
      <RelatedPosts tags={post.tags} />
      {/* <RelatedTasks post={post} /> */}
    </div>
  </footer>
);

const Post: React.FC<{ data: any }> = ({ data }) => {
  const post = data.graphCmsPost;
  return (
    <Layout bgCircles={false}>
      {post.featuredImage && (
        <Banner style={{ backgroundImage: `url("${post.featuredImage.url}` }} />
      )}
      <section
        className={styles.section}
        style={{ top: 400, marginBottom: 400 /* add height of banner*/ }}
      >
        <Header post={post} />
        <Markdown>{post.content?.markdown || ""}</Markdown>
        <Footer post={post} />
      </section>
    </Layout>
  );
};

export default Post;

export const query = graphql`
  query($id: String!) {
    graphCmsPost(id: { eq: $id }) {
      id
      createdAt
      title
      featuredImage {
        url
      }
      content {
        markdown
      }
      category {
        id
        title
      }
      tags
    }
  }
`;
