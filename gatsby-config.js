require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});

module.exports = {
  siteMetadata: {
    title: `The Global Sustainability Project`,
    siteUrl: `https://www.tgsp.earth`,
    description: `Sustainability for everyone.`,
  },
  plugins: [
    `gatsby-plugin-typescript`,
    `gatsby-plugin-antd`,
    `gatsby-plugin-sass`,
    {
      resolve: "gatsby-source-graphcms",
      options: {
        endpoint: process.env.GATSBY_API_ENDPOINT,
        token: process.env.GATSBY_API_TOKEN,
      },
    },
    {
      resolve: `gatsby-plugin-sitemap`,
      options: {
        output: `/sitemap.xml`,
      },
    },
  ],
};
