const path = require(`path`);

const slugify = require("slugify");

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions;
  const Post = path.resolve("./src/templates/Post.tsx");
  const Task = path.resolve("./src/templates/Task.tsx");

  return graphql(`
    {
      allGraphCmsPost {
        edges {
          node {
            title
            id
            category {
              title
            }
          }
        }
      }
      allGraphCmsTask {
        edges {
          node {
            title
            id
          }
        }
      }
    }
  `).then((result) => {
    if (result.errors) throw result.errors;
    else
      return result.data.allGraphCmsPost.edges.forEach((post) => {
        const categorySlug = slugify(
          post.node.category.length ? post.node.category[0].title : "category"
        ).toLowerCase();
        const titleSlug = slugify(post.node.title || "").toLowerCase();
        const postSlug = `${categorySlug}/${titleSlug}`;
        if (categorySlug && postSlug)
          createPage({
            path: postSlug,
            component: Post,
            context: {
              id: post.node.id,
            },
          });
      });
    // result.data.allGraphCmsTask.edges.forEach(task => {
    //   const taskSlug = `badges/${task.node.title}`
    //   if (taskSlug)
    //     createPage({
    //       path: taskSlug,
    //       component: Task,
    //       context: {
    //         id: task.node.id,
    //       },
    //     })
    // })
  });
};

// https://spectrum.chat/gatsby-js/general/having-issue-related-to-chunk-commons-mini-css-extract-plugin~0ee9c456-a37e-472a-a1a0-cc36f8ae6033?m=MTU3MjYyNDQ5OTAyNQ==
exports.onCreateWebpackConfig = ({ stage, actions, getConfig }) => {
  if (stage === "build-javascript") {
    const config = getConfig();
    const miniCssExtractPlugin = config.plugins.find(
      (plugin) => plugin.constructor.name === "MiniCssExtractPlugin"
    );
    if (miniCssExtractPlugin) {
      miniCssExtractPlugin.options.ignoreOrder = true;
    }

    actions.replaceWebpackConfig(config);
  }
};
